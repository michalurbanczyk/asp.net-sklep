﻿<%@ Page Title="Kontakt" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Projekt_gotowy.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <h2><%: Title %></h2>
    <h3>Strona kontaktowa</h3>
    <address>
        Kraków<br />
        Radzikowskiego, 74<br />
        <abbr title="Phone">P:</abbr>
        794742919
    </address>

    <address>
        <strong>Pomoc technicza:</strong>   <a href="mailto:Support@example.com">murbanczyk1996@outlook.com</a><br />
    </address>

</asp:Content>
