﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class Zamowienie : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			Response.Redirect("Panel_klienta.aspx");
		}

		protected void btnPowrot_Click(object sender, EventArgs e)
		{
			int wybrany_produkt = int.Parse(RadioButtonList1.SelectedValue);

			int ilosc = int.Parse(TextBox1.Text);

			SqlConnection con = new SqlConnection();
			con.ConnectionString = ConfigurationManager.ConnectionStrings["podlaczenie_do_bazy"].ToString();
			SqlCommand cmd = new SqlCommand("select Produkty.Ilosc from Produkty where Produkty.ID=@ID", con);

			SqlCommand cmd_cena = new SqlCommand("select Produkty.Cena from Produkty where Produkty.ID=@ID", con);

			cmd.Parameters.AddWithValue("@ID", wybrany_produkt);
			cmd_cena.Parameters.AddWithValue("@ID", wybrany_produkt);

			con.Open();
			int ilosc_dostepnych = (int)cmd.ExecuteScalar();
			double cena_prod = Convert.ToDouble(cmd_cena.ExecuteScalar());


			con.Close();

			if (ilosc_dostepnych < ilosc || ilosc < 1 )
			{
				Label2.Text = "Brak takiej ilości na magazynie lub wpisales ujemna wartosc!";
			}
			else
			{
				double podsumowanie = cena_prod * ilosc; 

				Label2.Text = "Zamowienie złożone!" + "Do zapłaty: " + podsumowanie + " zł";
				SqlCommand cmd1 = new SqlCommand("select ID from Klient where login=@Login", con);
				cmd1.Parameters.AddWithValue("@Login", Session["Login"]);

				con.Open();
				int id_klienta = (int)cmd1.ExecuteScalar();
				con.Close();

				SqlCommand cmd2 = new SqlCommand("insert into Zamowienie ([ID.Klient],[ID.Produkty],Ilosc_sztuk,Data_zamowienia,Do_zaplaty) values (@ID_Klient,@ID_Produkt,@ilosc,getdate(),@Koszt)", con);

				cmd2.Parameters.AddWithValue("@ID_Produkt", wybrany_produkt);
				cmd2.Parameters.AddWithValue("@ID_Klient", id_klienta);
				cmd2.Parameters.AddWithValue("@ilosc", ilosc);
				cmd2.Parameters.AddWithValue("@Koszt", podsumowanie);
				

				con.Open();
				cmd2.ExecuteNonQuery();
				con.Close();

				int nowa_ilosc = ilosc_dostepnych - ilosc;
				SqlCommand cmd3 = new SqlCommand("update Produkty set Ilosc=@Nowa_ilosc where ID=@Id", con);
				cmd3.Parameters.AddWithValue("@Id", wybrany_produkt);
				cmd3.Parameters.AddWithValue("@Nowa_ilosc", nowa_ilosc);

				con.Open();
				cmd3.ExecuteNonQuery();
				con.Close();

			}
		}
	}
}