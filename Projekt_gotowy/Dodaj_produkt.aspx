﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dodaj_produkt.aspx.cs" Inherits="Projekt_gotowy.Dodaj_produkt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align:center">
            <b style="font-size: xx-large">Dodawanie Produktu</b>
        </div>
    <div>

        <table class="nav-justified" style="width: 22%; height: 172px; margin-bottom: 74px">
            <tr>
                <td class="text-center" colspan="3"><strong>Wprowadzanie nowego produktu</strong></td>
            </tr>
            <tr>
                <td style="height: 41px">Nazwa:</td>
                <td style="height: 41px"></td>
                <td style="width: 144px; height: 41px">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 42px">Ilość:</td>
                <td style="height: 42px"></td>
                <td style="width: 144px; height: 42px">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Cena:</td>
                <td>&nbsp;</td>
                <td style="width: 144px">
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">
                    <asp:Label ID="LabelR" runat="server" ForeColor="Red" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="text-right" colspan="3">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Dodaj produkt" />
                </td>
            </tr>
        </table>
        <br />
        <p>
        <strong>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" style="font-size: xx-large" Text="Powróć do Panelu Administacyjnego" Width="545px" />
        </strong>
        </p>

    </div>
</asp:Content>
