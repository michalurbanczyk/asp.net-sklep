﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rejestracja.aspx.cs" Inherits="Projekt_gotowy.Rejestracja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align:center" class="auto-style3">
            <b style="font-size: x-large">
            REJESTRACJA</b>
        </div>

      <div style="font-family: Arial; align-content:center">
        <table style="border: 1px solid black; height: 296px;">
            <tr>
                <td colspan="2" class="text-center" style="font-size: x-large; height: 45px">
                    <strong>Panel rejestacji:</strong></td>
            </tr>
            <tr>
                <td class="text-right">
                    Podaj login:
                </td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputUserName" runat="server">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Podaj haslo:</td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputPassword" TextMode="Password" runat="server">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Powtorz haslo:&nbsp; </td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputPassword2" TextMode="Password" runat="server">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Imie</td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputUserImie" runat="server" Width="128px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Nazwisko</td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputUserNazwisko" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Adres</td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputUserAdres" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Kod pocztowy</td>
                <td style="width: 250px">
                    <asp:TextBox ID="InputUserNIP" runat="server"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td colspan="2" class="text-center">
                    <asp:Button ID="btnRegister" runat="server" Text="Zarejestruj sie!" 
                        onclick="btnRegister_Click" />
                </td>
            </tr>
            
             <tr>
                <td colspan="2" class="text-center">
                    <asp:Label ID="LabelR" runat="server" Text="" ForeColor ="Red"></asp:Label>
                </td>
            </tr>
            
        </table>
    </div>
        <p>
                    &nbsp;</p>
    <p class="text-center">
                    <asp:Button ID="btnPowrot" runat="server" Text="Powrót" 
                        onclick="btnPowrot_Click" style="font-size: medium" />
                </p>
</asp:Content>
