﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Nasza_oferta.aspx.cs" Inherits="Projekt_gotowy.Nasza_oferta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p style="font-size: x-large">
    &nbsp;</p>
<p style="font-size: x-large">
    <strong>Aktualny stan magazynowy:</strong></p>
<p>
    <asp:DataList ID="DataList1" runat="server" BackColor="White" BorderColor="Black" BorderWidth="12px" CellPadding="1" CellSpacing="5" DataSourceID="Lista_magazynu" Font-Bold="False" Font-Italic="False" Font-Names="Comic Sans MS" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="Black" GridLines="Both">
        <FooterStyle BackColor="#C6C3C6" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="#E7E7FF" />
        <ItemStyle BackColor="#DEDFDE" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
        <ItemTemplate>
            Nazwa produktu:
            <asp:Label ID="NazwaLabel" runat="server" Text='<%# Eval("Nazwa") %>' />
            <br />
            Ilość na stanie:
            <asp:Label ID="IloscLabel" runat="server" Text='<%# Eval("Ilosc") %>' />
            <br />
            Cena za 1 szt:
            <asp:Label ID="CenaLabel" runat="server" Text='<%# Eval("Cena") %>' />
            &nbsp;zł<br />
<br />
        </ItemTemplate>
        <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
    </asp:DataList>
    <asp:SqlDataSource ID="Lista_magazynu" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="SELECT [Nazwa], [Ilosc], [Cena] FROM [Produkty]"></asp:SqlDataSource>
</p>
</asp:Content>
