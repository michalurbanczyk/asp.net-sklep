﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class AMagazyn : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void Button2_Click(object sender, EventArgs e)
		{
			Response.BufferOutput = true;
			Response.Redirect("Dodaj_produkt.aspx");
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			Session.Clear();
			Session.Abandon();
			FormsAuthentication.SignOut();
			Response.Redirect("Logowanie.aspx");
		}
	}
}