﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Moje_zamowienia.aspx.cs" Inherits="Projekt_gotowy.Moje_zamowienia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label1" runat="server" BorderColor="#666666" BorderStyle="Groove" BorderWidth="2px" Font-Bold="True" Font-Names="OCR A Extended" Font-Size="XX-Large" ForeColor="Black" Text="Twoje Zamowienia"></asp:Label>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="klient_zam" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                <asp:BoundField DataField="Kod_pocztowy" HeaderText="Kod_pocztowy" SortExpression="Kod_pocztowy" />
                <asp:BoundField DataField="Nazwa" HeaderText="Nazwa" SortExpression="Nazwa" />
                <asp:BoundField DataField="Ilosc_sztuk" HeaderText="Ilosc_sztuk" SortExpression="Ilosc_sztuk" />
                <asp:BoundField DataField="Data_zamowienia" HeaderText="Data_zamowienia" SortExpression="Data_zamowienia" />
                <asp:BoundField DataField="Do_zaplaty" HeaderText="Do_zaplaty" SortExpression="Do_zaplaty" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="klient_zam" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="select Klient.Imie,Klient.Nazwisko,Klient.Adres,Klient.Kod_pocztowy,Produkty.Nazwa,Zamowienie.Ilosc_sztuk,Zamowienie.Data_zamowienia, Zamowienie.Do_zaplaty from Zamowienie join Klient on Klient.ID=Zamowienie.[ID.Klient] join Produkty on Produkty.ID=Zamowienie.[ID.Produkty] where Klient.login =  @Login">

            <SelectParameters>
                <asp:SessionParameter Name="Login" SessionField="Login" />
            </SelectParameters>
        </asp:SqlDataSource>
    </p>
    
        <p>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wstecz" />
    </p>

   

</asp:Content>
