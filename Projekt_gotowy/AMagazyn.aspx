﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AMagazyn.aspx.cs" Inherits="Projekt_gotowy.AMagazyn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyloguj" />

    <hr style="margin-bottom: 0px" />
    <div class="navbar-collapse collapse">
     <ul class="nav navbar-nav">
                  <li><a runat="server" href="~/AKlienci">Klienci</a></li>
                  <li><a runat="server" href="~/AMagazyn">Magazyn</a></li>
                  <li><a runat="server" href="~/AZamowienia">Zamówienia</a></li>
     </ul>
   </div>
    <asp:Label ID="Label1" runat="server" Text="Magazyn" CssClass="active" Font-Bold="True" Font-Names="Felix Titling" Font-Size="XX-Large" Font-Strikeout="False" Font-Underline="False" ForeColor="#009933" TextAlign="Center"></asp:Label>
     <br />
     <br />
     <strong>
     <asp:Button ID="Button2" runat="server" style="font-weight: bold; font-size: x-large" Text="Dodaj nowy produkt" Width="241px" OnClick="Button2_Click" />
     <br />
     <br />
     </strong>

    <div>


        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="ID" DataSourceID="MagazynAll">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="Nazwa" HeaderText="Nazwa" SortExpression="Nazwa" />
                <asp:BoundField DataField="Ilosc" HeaderText="Ilosc" SortExpression="Ilosc" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" SortExpression="Cena" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
        <asp:SqlDataSource ID="MagazynAll" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="SELECT [Nazwa], [Ilosc], [Cena], [ID] FROM [Produkty]"></asp:SqlDataSource>
        <br />
        Szczegóły wybranego produktu:<br />
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="ID" DataSourceID="MagazynOne" Height="50px" Width="125px">
            <EditRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <Fields>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="Nazwa" HeaderText="Nazwa" SortExpression="Nazwa" />
                <asp:BoundField DataField="Ilosc" HeaderText="Ilosc" SortExpression="Ilosc" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" SortExpression="Cena" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="MagazynOne" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" DeleteCommand="DELETE FROM [Produkty] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Produkty] ([Nazwa], [Ilosc], [Cena]) VALUES (@Nazwa, @Ilosc, @Cena)" SelectCommand="SELECT * FROM [Produkty] WHERE ([ID] = @ID)" UpdateCommand="UPDATE [Produkty] SET [Nazwa] = @Nazwa, [Ilosc] = @Ilosc, [Cena] = @Cena WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Nazwa" Type="String" />
                <asp:Parameter Name="Ilosc" Type="Int32" />
                <asp:Parameter Name="Cena" Type="Double" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Nazwa" Type="String" />
                <asp:Parameter Name="Ilosc" Type="Int32" />
                <asp:Parameter Name="Cena" Type="Double" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>


    </div>
</asp:Content>
