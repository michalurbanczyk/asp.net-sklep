﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class Panel_klienta : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string query = "SELECT Klient.Imie from Klient where login = @Login";


			SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["podlaczenie_do_bazy"].ToString());
			sqlConn.Open();
			SqlCommand cmd = new SqlCommand(query, sqlConn);

			cmd.Parameters.AddWithValue("@Login", Session["Login"]);

			SqlDataReader rdr = cmd.ExecuteReader();
			while (rdr.Read())
			{
				string column = rdr["Imie"].ToString();
				Label1.Text = "Witaj " + column + ". Zapraszamy do złożenia zamówienia";

			}
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			Session.Clear();
			Session.Abandon();
			FormsAuthentication.SignOut();
			Response.Redirect("Logowanie.aspx");
		}
	}
}