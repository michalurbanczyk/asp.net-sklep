﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Logowanie.aspx.cs" Inherits="Projekt_gotowy.Logowanie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="font-family: Arial; align-content:center">
      <h2>
      <strong> Logowanie użytkownika </strong>
      </h2>
        <table style="border: 1px solid black">
            <tr>
                <td colspan="2">
                    <b>Login</b>
                </td>
            </tr>
            <tr>
                <td>
                   Nazwa Użytkownika
                </td>
                <td>
                    <asp:TextBox ID="txtUserNameU" runat="server">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Hasło
                </td>
                <td>
                    <asp:TextBox ID="txtPasswordU" TextMode="Password" runat="server">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="chkBoxRememberMe2" Text="Zapamiętaj mnie" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnLoginU" runat="server" Text="Login" onclick="btnLoginU_Click" />
                </td>
            </tr>
             <tr>
                <td colspan="2">
                    <asp:Label ID="LabelU" runat="server" ForeColor ="Red" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            
        </table>
        <br />
       <p>
                    <asp:Button ID="btnPowrot" runat="server" Text="Strona Domowa" 
                        onclick="btnPowrot_Click" />
                </p>
        
    </div>
        <br />
        <div>
        Nie masz konta? Zarejestruj sie <a href="Rejestracja.aspx">tutaj</a>!
        </div>
</asp:Content>
