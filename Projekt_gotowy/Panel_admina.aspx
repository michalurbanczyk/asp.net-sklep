﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Panel_admina.aspx.cs" Inherits="Projekt_gotowy.Panel_admina" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p class="text-center" style="font-size: xx-large">
        <strong>Witaj w Panelu Administatora!</strong></p>
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyloguj" />

    <hr style="margin-bottom: 0px" />
    <div class="navbar-collapse collapse">
     <ul class="nav navbar-nav">
                  <li><a runat="server" href="~/AKlienci">Klienci</a></li>
                  <li><a runat="server" href="~/AMagazyn">Magazyn</a></li>
                  <li><a runat="server" href="~/AZamowienia">Zamówienia</a></li>
     </ul>
   </div>
</asp:Content>
