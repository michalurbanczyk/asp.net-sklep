﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Zamowienie.aspx.cs" Inherits="Projekt_gotowy.Zamowienie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align:center; font-size: x-large;">
        <span style="font-weight: bold">
            <br /><span style="font-size: xx-large">Złóż zamówienie<br />
            </span>
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="#00CC00"></asp:Label>    

        </span>
    </div>

    <p style="font-size: x-large">
        
        Lista produktów:</p>
    <p>
        
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" DataSourceID="produkty" DataTextField="Nazwa" DataValueField="ID">
        </asp:RadioButtonList>
        <asp:SqlDataSource ID="produkty" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="SELECT [Nazwa], [ID] FROM [Produkty]"></asp:SqlDataSource>
        
    </p>

    <p>
        Podaj ilość sztuk:
    </p>
    <p>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </p>

     <p>
        <span style="font-weight: bold">
            <asp:Button ID="btnPowrot" runat="server" Text="Złóż zamowienie" OnClick="btnPowrot_Click" BorderColor="#000066" BorderStyle="Solid" Font-Size="XX-Large" Height="61px" Width="410px" 
                        />
        </span>
    </p>
    <p>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wstecz" />
    </p>
</asp:Content>
