﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AKlienci.aspx.cs" Inherits="Projekt_gotowy.AKlienci" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyloguj" />

    <hr style="margin-bottom: 0px" />
    <div class="navbar-collapse collapse">
     <ul class="nav navbar-nav">
                  <li></li>
                  <li><a runat="server" href="~/AKlienci">Klienci</a></li>
                  <li><a runat="server" href="~/AMagazyn">Magazyn</a></li>
                  <li><a runat="server" href="~/AZamowienia">Zamówienia</a></li>
                  
     </ul>
   </div>
    <hr style="margin-top: 10px; margin-bottom: 0px" />
    <p>
    <asp:Label ID="Label1" runat="server" Text="KLIENCI" BackColor="White" BorderColor="#33CC33" BorderWidth="5px" Enabled="False" Font-Bold="True" Font-Italic="True" Font-Size="XX-Large"></asp:Label>
    </p>
    <div>

        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="ID" DataSourceID="AdminKlient">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                <asp:BoundField DataField="Kod_pocztowy" HeaderText="Kod_pocztowy" SortExpression="Kod_pocztowy" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#002876" />
        </asp:GridView>
        <asp:SqlDataSource ID="AdminKlient" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="SELECT [Imie], [Nazwisko], [Adres], [Kod_pocztowy], [ID] FROM [Klient]"></asp:SqlDataSource>

        <br />
        Szczegółowe dane o kliencie:<br />
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" DataKeyNames="ID" DataSourceID="KlientDane" ForeColor="Black" Height="50px" Width="125px">
            <EditRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <Fields>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                <asp:BoundField DataField="Kod_pocztowy" HeaderText="Kod_pocztowy" SortExpression="Kod_pocztowy" />
                <asp:BoundField DataField="login" HeaderText="login" SortExpression="login" />
                <asp:BoundField DataField="haslo" HeaderText="haslo" SortExpression="haslo" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="KlientDane" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" DeleteCommand="DELETE FROM [Klient] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Klient] ([Imie], [Nazwisko], [Adres], [Kod_pocztowy], [login], [haslo]) VALUES (@Imie, @Nazwisko, @Adres, @Kod_pocztowy, @login, @haslo)" SelectCommand="SELECT * FROM [Klient] WHERE ([ID] = @ID)" UpdateCommand="UPDATE [Klient] SET [Imie] = @Imie, [Nazwisko] = @Nazwisko, [Adres] = @Adres, [Kod_pocztowy] = @Kod_pocztowy, [login] = @login, [haslo] = @haslo WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Imie" Type="String" />
                <asp:Parameter Name="Nazwisko" Type="String" />
                <asp:Parameter Name="Adres" Type="String" />
                <asp:Parameter Name="Kod_pocztowy" Type="String" />
                <asp:Parameter Name="login" Type="String" />
                <asp:Parameter Name="haslo" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Imie" Type="String" />
                <asp:Parameter Name="Nazwisko" Type="String" />
                <asp:Parameter Name="Adres" Type="String" />
                <asp:Parameter Name="Kod_pocztowy" Type="String" />
                <asp:Parameter Name="login" Type="String" />
                <asp:Parameter Name="haslo" Type="String" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>
