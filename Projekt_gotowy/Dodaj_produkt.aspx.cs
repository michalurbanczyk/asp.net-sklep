﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class Dodaj_produkt : System.Web.UI.Page
	{
		SqlConnection con = new SqlConnection();
		protected void Page_Load(object sender, EventArgs e)
		{
			
			con.ConnectionString = ConfigurationManager.ConnectionStrings["podlaczenie_do_bazy"].ToString();
			con.Open();
		}

		protected void Button2_Click(object sender, EventArgs e)
		{
			Response.BufferOutput = true;
			Response.Redirect("Panel_admina.aspx");
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			SqlCommand cmd = new SqlCommand("insert into Produkty (Nazwa,Ilosc,Cena) values (@Nazwa_produktu,@Ilosc,@Cena)", con);


			cmd.Parameters.AddWithValue("Nazwa_produktu", TextBox1.Text);
			cmd.Parameters.AddWithValue("Ilosc", TextBox2.Text);
			cmd.Parameters.AddWithValue("Cena", double.Parse(TextBox3.Text));

			cmd.ExecuteNonQuery();


			LabelR.Text = "Pomyslnie dodano!";
		}
	}
}