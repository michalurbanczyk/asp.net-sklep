﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class Rejestracja : System.Web.UI.Page
	{
		SqlCommand cmd = new SqlCommand();
		SqlConnection con = new SqlConnection();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["Login"] != null)
			{
				string tmp = Session["Login"].ToString();
				if (tmp == "admin")
				{
					Response.Redirect("Panel_admina.aspx");
				}
				else
				{
					Response.Redirect("Panel_klienta.aspx");
				}

			}

			con.ConnectionString = ConfigurationManager.ConnectionStrings["podlaczenie_do_bazy"].ToString();
			con.Open();
		}

		protected void btnPowrot_Click(object sender, EventArgs e)
		{
			Response.BufferOutput = true;
			Response.Redirect("Default.aspx");
		}

		protected void btnRegister_Click(object sender, EventArgs e)
		{
			cmd = new SqlCommand("insert into Klient" + "(login,haslo,Imie,Nazwisko,Adres,Kod_pocztowy) values (@Login,@Password,@Imie,@Nazwisko,@Adres,@KodPocztowy)", con);


			cmd.Parameters.AddWithValue("Login", InputUserName.Text);
			cmd.Parameters.AddWithValue("Password", InputPassword.Text);
			cmd.Parameters.AddWithValue("Password2", InputPassword2.Text);
			cmd.Parameters.AddWithValue("Imie", InputUserImie.Text);
			cmd.Parameters.AddWithValue("Nazwisko", InputUserNazwisko.Text);
			cmd.Parameters.AddWithValue("Adres", InputUserAdres.Text);
			cmd.Parameters.AddWithValue("KodPocztowy", InputUserNIP.Text);

			cmd.ExecuteNonQuery();

			LabelR.Text = "Pomyslnie dodano!";
		}
	}
}