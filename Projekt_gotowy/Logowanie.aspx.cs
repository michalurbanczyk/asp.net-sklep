﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projekt_gotowy
{
	public partial class Logowanie : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(Session["Login"] != null)
			{
				string tmp = Session["Login"].ToString();
				if(tmp == "admin")
				{
					Response.Redirect("Panel_admina.aspx");
				}
				else
				{
					Response.Redirect("Panel_klienta.aspx");
				}
				
			}
		}

		protected void btnLoginU_Click(object sender, EventArgs e)
		{
			SqlConnection con = new SqlConnection();
			con.ConnectionString = ConfigurationManager.ConnectionStrings["podlaczenie_do_bazy"].ToString();
			SqlCommand cmd = new SqlCommand("select * from [Klient] where login=@Login and haslo=@Password", con);
			cmd.Parameters.AddWithValue("@Login", txtUserNameU.Text);
			cmd.Parameters.AddWithValue("@Password", txtPasswordU.Text);

			SqlDataAdapter sda = new SqlDataAdapter(cmd);
			DataTable dt = new DataTable();
			sda.Fill(dt);

			con.Open();


			int i = cmd.ExecuteNonQuery();
			con.Close();


			if (dt.Rows.Count > 0)
			{
				Session["Login"] = txtUserNameU.Text;
				Session["haslo"] = txtPasswordU.Text;
				if(txtUserNameU.Text == "admin")
				{
					Response.Redirect("Panel_admina.aspx");
				}
				else
				{
					Response.Redirect("Panel_klienta.aspx");
				}
				
				//Session.RemoveAll();
				//TO przy wylogowaniu
			}
			else
			{
				LabelU.Text = "Niepoprawny login/haslo!";
			}
		}

		protected void btnPowrot_Click(object sender, EventArgs e)
		{
			Response.BufferOutput = true;
			Response.Redirect("Default.aspx");
		}
	}
}