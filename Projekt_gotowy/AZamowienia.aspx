﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AZamowienia.aspx.cs" Inherits="Projekt_gotowy.AZamowienia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


     <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Wyloguj" />

    <hr style="margin-bottom: 0px" />
    <div class="navbar-collapse collapse">
     <ul class="nav navbar-nav">
                  <li><a runat="server" href="~/AKlienci">Klienci</a></li>
                  <li><a runat="server" href="~/AMagazyn">Magazyn</a></li>
                  <li><a runat="server" href="~/AZamowienia">Zamówienia</a></li>
     </ul>
   </div>

    <div>

        <span style="font-size: large; text-decoration: underline">Klienci:</span><asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="ID" DataSourceID="Klienci_dane" ForeColor="Black" GridLines="Vertical">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                <asp:BoundField DataField="Kod_pocztowy" HeaderText="Kod_pocztowy" SortExpression="Kod_pocztowy" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>
        <asp:SqlDataSource ID="Klienci_dane" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="SELECT [Imie], [Nazwisko], [Adres], [Kod_pocztowy], [ID] FROM [Klient]"></asp:SqlDataSource>
        <br style="font-size: x-large" />
        <span style="font-size: large; text-decoration: underline">Zamówienie wybranego klienta:<br />
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="ID" DataSourceID="KlientOne" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                <asp:BoundField DataField="Nazwa" HeaderText="Nazwa" SortExpression="Nazwa" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" SortExpression="Cena" Visible="False" />
                <asp:BoundField DataField="Ilosc_sztuk" HeaderText="Ilosc_sztuk" SortExpression="Ilosc_sztuk" />
                <asp:BoundField DataField="Data_zamowienia" HeaderText="Data_zamowienia" SortExpression="Data_zamowienia" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
                <asp:BoundField DataField="Do_zaplaty" HeaderText="Do_zaplaty" SortExpression="Do_zaplaty" />
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
        <asp:SqlDataSource ID="KlientOne" runat="server" ConnectionString="<%$ ConnectionStrings:podlaczenie_do_bazy %>" SelectCommand="select Klient.Imie,Klient.Nazwisko,Produkty.Nazwa,Produkty.Cena,Zamowienie.Ilosc_sztuk,Zamowienie.Data_zamowienia,Zamowienie.ID, Zamowienie.Do_zaplaty from Zamowienie join Klient on Klient.ID=Zamowienie.[ID.Klient] join Produkty on Produkty.ID=Zamowienie.[ID.Produkty] where Zamowienie.[ID.Klient]=@IdKlient
">
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="IdKlient" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        </span>

    </div>


</asp:Content>
